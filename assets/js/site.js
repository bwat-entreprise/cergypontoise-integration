$(document).ready(function(){
	$('#close').click(function(){
		$('.top-text').slideUp();
	});

	//is mobile function
	function isMobile() {
		var screensize = document.documentElement.clientWidth;
		if (screensize  <= 1024) {
			return true;
		}
		return false;
	}


	//menu mobile
	$('li.dropdown').click(function(){
		$(this).find('.dropdown-content').toggle();
		$(this).toggleClass('clicked');
	});


	/* menu mobile */

	$('.menu-mobile').click(function(){
		$('.menu-content').toggleClass('active');
		$('nav').toggleClass('active');
		$('nav ul').toggle();
	});

	if (!isMobile()){
		//menu scroll
		$(window).scroll(function(){
			if ($('body').is('#home')){
				if ($(window).scrollTop() >= 650){
					$('nav').addClass('fixed');
				}
				else if ($(window).scrollTop() < 650){
					$('nav').removeClass('fixed');
				}
			}

			if ($(window).scrollTop() >= 80){
				$('#page nav').addClass('fixed');
			}
			else if ($(window).scrollTop() < 80){
				$('nav').removeClass('fixed');
			}
			if ($(window).scrollTop() >= 50) {
				$('.top-text').slideUp();
			}
		});
	}


	//accordion page
	$('.accordion .hidden').hide();
	$('.accordion .bloc').click(function(){
		$(this).toggleClass('active');
		$(this).find('.hidden').slideToggle();
	});

	// recherche box
	$('.search-box').hide();
	$('nav li.search').click(function(){
		$('nav ul').hide();
		$('.search-box').show();
	});

	$('.search-box .croix').click(function(){
		$('nav ul').show();
		$('.search-box').hide();
		return false;
	});

	/* slick banner home*/
		// ----------------------
		// SLIDER INIT
		// ----------------------

		$( '.banner .slider' ).slick( {
			autoplay  	    : true,
			pauseOnHover    : false,
			arrows: true,
			prevArrow: false,
			nextArrow: false
		} );

		// ----------------------
		// NEXT BUTTON
		// ----------------------
		$( '.banner .sliderNext' ).click( function () {
			$( '.banner .slider' ).slick( 'slickNext' );
		} );

		// ----------------------
		// PREVIOUS BUTTON
		// ----------------------
		$( '.banner .sliderPrev' ).click( function () {
			$( '.banner .slider' ).slick( 'slickPrev' );
		} );

	/* carte interactif home */
	$('#OSNY').hover(function(){
		$('.osny').toggleClass('active');
	});
	$('.osny').hover(function(){
		$('#OSNY').toggleClass('active');
	});
	$('#PONTOISE').hover(function(){
		$('.pontoise').toggleClass('active');
	});
	$('.pontoise').hover(function(){
		$('#PONTOISE').toggleClass('active');
	});
	$('#SAINT-OUEN-L_x27_AUMONE').hover(function(){
		$('.saintouenlaumone').toggleClass('active');
	});
	$('.saintouenlaumone').hover(function(){
		$('#SAINT-OUEN-L_x27_AUMONE').toggleClass('active');
	});
	$('#PUISEUX-PONTOISE').hover(function(){
		$('.puiseux').toggleClass('active');
	});
	$('.puiseux').hover(function(){
		$('#PUISEUX-PONTOISE').toggleClass('active');
	});
	$('#ERAGNY').hover(function(){
		$('.eragny').toggleClass('active');
	});
	$('.eragny').hover(function(){
		$('#ERAGNY').toggleClass('active');
	});
	$('#CERGY').hover(function(){
		$('.cergy').toggleClass('active');
	});
	$('.cergy').hover(function(){
		$('#CERGY').toggleClass('active');
	});
	$('#NEUVILLE-SUR-OISE').hover(function(){
		$('.neuville').toggleClass('active');
	});
	$('.neuville').hover(function(){
		$('#NEUVILLE-SUR-OISE').toggleClass('active');
	});
	$('#VAUREAL').hover(function(){
		$('.vaureal').toggleClass('active');
	});
	$('.vaureal').hover(function(){
		$('#VAUREAL').toggleClass('active');
	});
	$('#JOUY-LE-MOUTIER').hover(function(){
		$('.jouylemoutier').toggleClass('active');
	});
	$('.jouylemoutier').hover(function(){
		$('#JOUY-LE-MOUTIER').toggleClass('active');
	});
	$('#MAURECOURT').hover(function(){
		$('.maurecourt').toggleClass('active');
	});
	$('.maurecourt').hover(function(){
		$('#MAURECOURT').toggleClass('active');
	});
	$('#COURDIMANCHE').hover(function(){
		$('.courdimanche').toggleClass('active');
	});
	$('.courdimanche').hover(function(){
		$('#COURDIMANCHE').toggleClass('active');
	});
	$('#MENUCOURT').hover(function(){
		$('.menucourt').toggleClass('active');
	});
	$('.menucourt').hover(function(){
		$('#MENUCOURT').toggleClass('active');
	});
	$('#BOISEMONT').hover(function(){
		$('.boisemont').toggleClass('active');
	});
	$('.boisemont').hover(function(){
		$('#BOISEMONT').toggleClass('active');
	});


	/* project slide */
	/* slick banner home*/
	// ----------------------
	// SLIDER INIT
	// ----------------------

	$( '.project .slider' ).slick( {
		autoplay  	    : true,
		pauseOnHover    : false,
		arrows: true,
		prevArrow: false,
		nextArrow: false,
		appendArrows: '.project .slider'
	} );

	// ----------------------
	// NEXT BUTTON
	// ----------------------
	$( '.project .sliderNext' ).click( function () {
		$( '.project .slider' ).slick( 'slickNext' );
	} );

	// ----------------------
	// PREVIOUS BUTTON
	// ----------------------
	$( '.project .sliderPrev' ).click( function () {
		$( '.project .slider' ).slick( 'slickPrev' );
	} );


	/*cookies*/

	$('.cookies .button-2').click(function(){
		$('.cookies').hide();
		return false;
	});

	/*$(window).click(function() {
		$('li.dropdown.clicked').removeClass('clicked');
	});

	$('li.dropdown-content').click(function(event){
		event.stopPropagation();
	});*/

});
