<!-- Bwat IG : Page d'accueil -->
<?php
    $class='home';
    include '../includes/header.php';
    include '../includes/partial/home/banner.html';
    include '../includes/menu.html';
    include '../includes/partial/home/access.html';
    include '../includes/partial/home/project.html';
    include '../includes/partial/home/actu.html';
    include '../includes/partial/home/sortir.html';
    include '../includes/partial/home/communes.html';
?>



    <!-- Bwat IG : Social networks -->
    <!--
    On utilise une solution js embed. On est pas au point sur le responsive de ce truc là
    On y re-travaillera après.
    -->
    <div class="reseau">
        <h2>EN RESEAUX</h2>
        <div id="wall" class="wrap">
            <div id="social-stream"></div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- Bwat IG : Partenaires  -->
    <!--
    Un partenaire c'est :
    - un nom
    - une image hauteur fixe 60px, largeur libre
    - un ordre d'affichage
    - un lien
    - une taget pour le lien (enum "_blank" ou "_parent")
    - un statut (published, archived..)
    -->
    <div class="partenary">
        <div class="wrap">
            <h2>NOS PARTENAIRES</h2>
            <div class="slider-nav">
                <div class="content">
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/logoCylumineFinal.png" alt="Cylumine" /></a></div>
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/logo-CYO.png" alt="CYO" /></a></div>
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/logo-veolia.png" alt="Véolia" /></a></div>
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/velo2.png" alt="Velo2" /></a></div>
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/stivo.png" alt="Stivo" /></a></div>
                    <div><a href="http://www.example.com" target="_blank"><img src="/assets/images/footer/logoCylumineFinal.png" alt="Cylumine" /></a></div>
                </div>
            </div>
        </div>
    </div>
<?php
    include '../includes/footer.php';
?>
