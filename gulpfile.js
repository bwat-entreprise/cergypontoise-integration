
var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    livereload   = require('gulp-livereload'),
    bourbon      =  require('node-bourbon').includePaths
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss    = require('gulp-minify-css'),
    uglify       = require('gulp-uglify'),
    concat       = require('gulp-concat')
	cmq 		 = require('gulp-combine-media-queries');
;


var paths = {
    scss: './assets/scss/**/*.scss'
    //html: './application/**/*.php'
};

gulp.task('scss', function () {
    return gulp.src([paths.scss])
        .pipe(sass({
        	errLogToConsole: true,
        	includePaths: ['scss'].concat([bourbon])
        }))
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('prefixer', ['scss'], function () {
    return gulp.src('./assets/css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./assets/css'));
});
/*
gulp.task('cmq', function () {
  gulp.src('./assets/dist/*.css')
    .pipe(cmq({
      log: true,
      use_external: true
    }))
    .pipe(gulp.dest('./assets/dist'));
});
*/
gulp.task('minify-css', ['prefixer'], function() {
  return gulp.src('./assets/css/*.css')
    //.pipe(cmq({log: true,use_external: true}))
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('./assets/dist'));
});

gulp.task('concat-js', ['scss'], function() {
  return gulp.src([
				'./assets/js/jquery-3.2.1.min.js',
                //'./assets/js/jquery-validation/jquery.validate.min.js',
				//'./assets/js/jquery-validation/localization/messages_fr.js',
				//'./assets/js/jquery-validation/localization/methods_fr.js',
                './assets/js/imagesloaded.pkgd.min.js',
                './assets/js/justified.js',
                './assets/js/lightbox.min.js',
                './assets/js/slick.js',
                './assets/js/site.js'
  
  ]) //'./assets/js/*.js')
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./assets/dist/'));
});
gulp.task('compress', ['concat-js'], function() {
  return gulp.src('./assets/dist/all.js')
      .pipe(uglify())
      .pipe(gulp.dest('./assets/dist'))
      .pipe(livereload());
});

gulp.task('default',function(){
    gulp.start(['scss', 'prefixer','minify-css','concat-js','compress']);
});

gulp.task('minification',function(){
    gulp.start('compress');
});



gulp.task('watch', function () {
	livereload.listen();
	gulp.watch([paths.scss], ['scss', 'prefixer', 'minify-css','concat-js','compress']);
});