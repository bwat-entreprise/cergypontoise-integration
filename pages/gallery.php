<?php
$class='page';
include '../includes/header.php';
include '../includes/menu.html';
?>

<div class="wrap page" id="page">
    <?php include '../includes/partial/pages/breadcrumb.html'; ?>
    <div class="page-gallery">
        <h1>Exemple de Galerie</h1>
        <!--
        Bwat IG : Gallery
        Une image de galerie c'est :
            - un titre (optionnel)
            - une date
            - une vignette format libre compris entre 580px par 580px (ex: 200x580, 580x400 ...)
            - une image format libre comprise entre 1160 par 1160px

        -->
        <div class="gallery">
            <div id="grid-container">
                <a class="example-image-link" href="/assets/images/actu-1.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                    <img class="grid-item" src="/assets/images/actu-1.png">
                </a>
                <a class="example-image-link" href="https://unsplash.it/1200/600?random" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                    <img class="grid-item" src="https://unsplash.it/1200/600?random">
                </a>
                <a class="example-image-link" href="https://unsplash.it/800/500?random" data-lightbox="example-set">
                    <img class="grid-item" src="https://unsplash.it/800/500?random">
                </a>
                <a class="example-image-link" href="https://unsplash.it/600/1200?random" data-lightbox="example-set">
                    <img class="grid-item" src="https://unsplash.it/600/1200?random">
                </a>
                <a class="example-image-link" href="https://unsplash.it/600/800?random" data-lightbox="example-set">
                    <img class="grid-item" src="https://unsplash.it/600/800?random">
                </a>
                <a class="example-image-link" href="https://unsplash.it/800/450?random" data-lightbox="example-set">
                    <img class="grid-item" src="https://unsplash.it/800/450?random">
                </a>
                <a class="example-image-link" href="https://unsplash.it/1000/800?random" data-lightbox="example-set">
                    <img class="grid-item" src="https://unsplash.it/1000/800?random">
                </a>
            </div>
        </div>
    </div>
</div>

<?php

include '../includes/partial/pages/more.php';
include '../includes/footer.php';
?>
