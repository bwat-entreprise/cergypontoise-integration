<?php
$class='page';
include '../includes/header.php';
include '../includes/menu.html';
?>

<div class="wrap page search-result">
    <?php include '../includes/partial/pages/breadcrumb.html'; ?>
    <div class="page-content">
        <h1>Votre recherche</h1>

        <!-- formulaire de recherche -->
        <div class="result">
            <h2>Mot(s) clé(s) :</h2>
            <form action="#">
                <span class="search-content">
                    <input type="text" name="search" value="">
                    <img src="/assets/images/search.png" class="search-icon" alt="icône de recherche"/>
                </span>
                <a href=""><img src="/assets/images/croix.png" class="croix" alt="fermer recherche"/></a>
            </form>
        </div>
        <!-- Fin - formulaire de recherche -->

        <!-- Résultats de recherche -->
        <h2 class="title-result">Musique : <strong>15 résultats de recherche</strong></h2>
        <ul class="list-result">
            <li>
                <h3>Lorem Musique</h3>
                <p>ipsum lorem <em>musique</em> lorem ipsum lorem…</p>
            </li>
            <li>
                <h3>Lorem Musique</h3>
                <p>ipsum lorem <em>musique</em> lorem ipsum lorem…</p>
            </li>
            <li>
                <h3>Lorem Musique</h3>
                <p>ipsum lorem <em>musique</em> lorem ipsum lorem…</p>
            </li>
        </ul>
        <!-- Fin - Résultats de recherche -->
    </div>
</div>
<?php
include '../includes/footer.php';
?>
