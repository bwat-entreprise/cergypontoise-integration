<!-- Bwat IG : Gabarit deux colonnes -->
<?php
$class='page';
include '../includes/header.php';
include '../includes/menu.html';
?>

<div class="wrap page column2">
    <?php include '../includes/partial/pages/breadcrumb.html'; ?>

    <!-- Contenu de la Page -->
    <div class="page-content" id="form">
        <h1>Réservation de composteur pour habitat pavillonnaire</h1>
        <?php include '../includes/partial/pages/share.php'; ?>
        <div class="bloc-page">
            <div class="img-page" style="background-image:url('https://unsplash.it/900/500?random');"></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies non metus non dignissim. Aliquam erat volutpat. Nunc est neque, fermentum nec maximus a, faucibus non dui. Vestibulum vel mauris elementum, aliquam mi vel, convallis ex. </p>
            <form method="post" action="traitement.php">

                <fieldset class="global">
                    <label for="nom">Nom</label>
                    <input type="text" name="nom" id="nom" placeholder="Nom"/>

                    <label for="prenom">Prénom</label>
                    <input type="text" name="prenom" id="prenom" placeholder="Prénom"/>

                    <label for="adresse">Adresse</label>
                    <input type="text" name="adresse" id="adresse" placeholder="Adresse"/>
                    <div class="lieu">
                        <span>
                            <label for="code-postal">Code postal</label>
                            <input type="text" name="Code postal" id="code-postal" placeholder="Code postal"/>
                        </span>

                        <span>
                            <select name="ville" id="ville">
                                <option value="default">Ville</option>
                                <option value="france">Boisemont</option>
                                <option value="espagne">cergy</option>
                                <option value="italie">Courdimanche</option>
                                <option value="royaume-uni">Eragny-sur-Oise</option>
                                <option value="canada">Jouy-le-Moutier</option>
                                <option value="etats-unis">Maurecourt</option>
                                <option value="chine">Menucourt</option>
                                <option value="japon">Neuville-sur-Oise</option>
                            </select>
                        </span>
                    </div>

                </fieldset>

                <fieldset class="form-group">
                    <h3>Sélectionnez votre « RDV des déchets » préféré pour le retrait de votre composteur</h3>
                    <span>
                        <input type="radio" name="date" value="option1" id="option1" /> <label for="option1">Samedi 20 janvier 2018 entre 10h et 12h au Centre Technique Municipal d'Osny,  8/10 rue des Beaux-soleils </label>
                    </span>
                    <span>
                        <input type="radio" name="date" value="option2" id="option2" /> <label for="option2">Samedi 20 janvier 2018 entre 10h et 12h au Centre Technique Municipal d'Osny,  8/10 rue des Beaux-soleils </label>
                    </span>
                    <span>
                        <input type="radio" name="date" value="option3" id="option3" /> <label for="option3">Samedi 20 janvier 2018 entre 10h et 12h au Centre Technique Municipal d'Osny,  8/10 rue des Beaux-soleils </label>
                    </span>
                </fieldset>

                <fieldset class="form-group">
                    <h3>Justificatif de domicile</h3>
                    <span>
                        <input type="file" name="file" id="file" class="inputfile"/>
                    </span>
                </fieldset>

                <fieldset class="form-group">
                    <h3>Cochez dans la liste les éléments que vous préférez</h3>
                    <span>
                        <input type="checkbox" name="item" value="item1" id="item1" /> <label for="item1">Item 1</label>
                    </span>
                    <span>
                        <input type="checkbox" name="item" value="item2" id="item2" /> <label for="item2">Item 2</label>
                    </span>
                    <span>
                        <input type="checkbox" name="item" value="item3" id="item3" /> <label for="item3">Item 3</label>
                    </span>
                </fieldset>
                <div class="submit">
                    <input type="submit" value="Envoyer">
                </div>
            </form>
        </div>
        <!-- Fin - Contenu de la Page -->

        <!-- contenu(s) additionnel(s) dynamique(s) -->
        <aside>
            <?php
            include '../includes/partial/aside/contact-simple.php';
            ?>
        </aside>
        <!-- Fin - contenu(s) additionnel(s) dynamique(s) -->
    </div>
</div>
<?php
include '../includes/partial/pages/more.php';
include '../includes/footer.php';
?>
