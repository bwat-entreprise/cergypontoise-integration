<!-- Bwat IG : Gabarit deux colonnes -->
<?php
$class='page';
include '../includes/header.php';
include '../includes/menu.html';
?>

<div class="wrap page column2">
    <?php include '../includes/partial/pages/breadcrumb.html'; ?>

    <!-- Contenu de la Page -->
    <div class="page-content">
        <h1>Piscine des Béthunes</h1>
        <?php include '../includes/partial/pages/share.php'; ?>
        <div class="bloc-page">
            <div class="img-page" style="background-image:url('/assets/images/photo-full.png');"></div>
            <div class="intro">
                <p>Située au nord du département des Yvelines, tout près du confluent de la Seine et de l'Oise, Maurecourt est une commune de 365 hectares nichée entre l'Oise, à l'est, et un plateau agricole abrité par la forêt de l'Hautil, à l'ouest.</p>
            </div>

            <p>Village de 300 habitants en 1900, Maurecourt compte aujourd'hui 4 385 habitants, mais a su garder,  au fil des années, son caractère de « village ». Un habitat à taille humaine, des petits commerces de qualité  en centre-ville et une vie associative, sportive et culturelle d'un remarquable dynamisme en font un lieu  où il fait bon vivre.</p>
            <h2>" Le bon vivre ensemble "</h2>
            <p>La vie locale est rythmée par de nombreuses manifestations, parmi lesquelles les célèbres Gargantuades de printemps, un événement où l'art, l'écriture et la gastronomie se rencontrent autour d'un marché du terroir, d'un festival de musique et d'un salon du livre gourmand.
                Par ailleurs, le village de Maurecourt invite à la promenade. Ainsi, on peut marcher sur les traces de la peintre impressionniste Berthe Morisot, qui y a séjourné et peint une quinzaine de toiles, marquées de l'empreinte du village. On peut aussi flâner sur les rives de l'Oise pour découvrir l'attachement de Maurecourt à la batellerie. En effet, jusqu'en 1986, la commune avait une activité de réparation et de construction navale. En 2004, la municipalité a décidé de reconquérir les berges de l'Oise et d'entreprendre des actions en faveur de la faune et de la flore. Elles ont conduit Maurecourt à remporter le trophée de « Capitale française de la biodiversité 2011 » au concours Natureparif, qui récompense la mise en œuvre de démarches exemplaires en matière de protection et de restauration de la biodiversité.</p>

            <!-- Module accordion -->
            <div class="accordion">
                <div class="bloc">
                    <h3>Horaires d’ouverture - période scolaire</h3>
                    <div class="hidden">
                        <p>A partir du 1er septembre 2017</p>
                        <ul class="horaires">
                            <li><strong>Lundi</strong>fermée</li>
                            <li><strong>Mardi</strong>Scolaires / Associations</li>
                            <li><strong>Mercredi</strong>13h - 16h</li>
                            <li><strong>Jeudi</strong>12h - 14h</li>
                            <li><strong>Vendredi</strong>Scolaires / Associations</li>
                            <li><strong>Samedi</strong>13h - 19h</li>
                            <li><strong>Dimanche</strong>10h - 13h</li>
                        </ul>
                    </div>
                </div>
                <div class="bloc">
                    <h3>Horaires d’ouverture - vacances de la Toussaint </h3>
                    <div class="hidden">
                        <ul>
                            <li>Fermeture de la caisse 30 minutes avant la fermeture de la piscine.</li>
                            <li>Évacuation des bassins 15 minutes avant la fermeture de l’établissement.</li>
                            <li>Validité des cartes d’abonnement : 18 mois à partir de la 1ère utilisation  (hors forfait annuel, trimestriel ou mensuel).</li>
                        </ul>
                        <h4>Caractéristiques</h4>
                        <p><strong>La piscine des Béthunes à Saint-Ouen-l’Aumône a dû fermer ses portes au public le 8 novembre dernier pour des raisons de sécurité.</strong></p>
                        <ul>
                            <li>Bassin sportif de 25 m x 10 m, 4 couloirs, profondeur de 0,90 m à 2,10 m.</li>
                            <li>Solarium extérieur engazonné.</li>
                            <li>Distributeurs de boissons, friandises</li>
                        </ul>
                        <h4>Les activités de la piscine </h4>
                    </div>
                </div>
            </div>
            <!-- Fin - Module accordion -->
        </div>
        <!-- Fin - Contenu de la Page -->

        <!-- contenu(s) additionnel(s) dynamique(s) -->
        <aside>
            <?php
                include '../includes/partial/aside/contact-with-map.php';
                include '../includes/partial/aside/bon-a-savoir.php';
                include '../includes/partial/aside/telecharger.php';

                include '../includes/partial/aside/text.php';
                include '../includes/partial/aside/links.php';
                include '../includes/partial/aside/highlight.php';
            ?>
        </aside>
        <!-- Fin - contenu(s) additionnel(s) dynamique(s) -->
    </div>
</div>
<?php
include '../includes/partial/pages/more.php';
include '../includes/footer.php';
?>
