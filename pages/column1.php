<!-- Bwat IG : Gabarit une colonne -->
<!--
NB : Les gabarits une colonne ont tout de même un <aside> avec la possibilité de mettre un bloc contact
-->
<?php
    $class='page';
    include '../includes/header.php';
    include '../includes/menu.html';
?>
    <!--
    Bwat IG : Page
    Une page c'est au moins :
        Un titre
        Une image de header :
            - taille fixe 1400px par 500px (pour les pages en 1 colonne)
            - taille fixe 900px par 500px (pour les pages en 2 colonne)
        Une image de vignette :
            - taille fixe 360px par 240px
        Une intro (optionel)
        Une texte
        Un choix "Page sommaire" oui / non. Si il est "oui", il faudra ajouter une classe "pageIndex" au body
        Un statut de publication

    Une page peut-être liée à des contenus additionnels (cf <aside>)

    Une page peut-être liée à une ou plusieurs pages choisies via le backend (cf includes/partial/pages/more.php)

    -->
    <div class="wrap page column1">
        <!-- Chemin suivi -->
        <?php include '../includes/partial/pages/breadcrumb.html'; ?>
        <div class="page-content">
            <!-- Contenu de la Page -->
            <h1>Maurecourt</h1>
            <?php include '../includes/partial/pages/share.php'; ?>
            <div class="img-page" style="background-image:url('/assets/images/photo-full.png');"></div>
            <div class="bloc-page">
                <div class="intro">
                    <p>Située au nord du département des Yvelines, tout près du confluent de la Seine et de l'Oise, Maurecourt est une commune de 365 hectares nichée entre l'Oise, à l'est, et un plateau agricole abrité par la forêt de l'Hautil, à l'ouest.</p>
                </div>

                <p>Village de 300 habitants en 1900, Maurecourt compte aujourd'hui 4 385 habitants, mais a su garder,  au fil des années, son caractère de « village ». Un habitat à taille humaine, des petits commerces de qualité  en centre-ville et une vie associative, sportive et culturelle d'un remarquable dynamisme en font un lieu  où il fait bon vivre.</p>
                <h2>" Le bon vivre ensemble "</h2>
                <p>La vie locale est rythmée par de nombreuses manifestations, parmi lesquelles les célèbres Gargantuades de printemps, un événement où l'art, l'écriture et la gastronomie se rencontrent autour d'un marché du terroir, d'un festival de musique et d'un salon du livre gourmand.
                    Par ailleurs, le village de Maurecourt invite à la promenade. Ainsi, on peut marcher sur les traces de la peintre impressionniste Berthe Morisot, qui y a séjourné et peint une quinzaine de toiles, marquées de l'empreinte du village. On peut aussi flâner sur les rives de l'Oise pour découvrir l'attachement de Maurecourt à la batellerie. En effet, jusqu'en 1986, la commune avait une activité de réparation et de construction navale. En 2004, la municipalité a décidé de reconquérir les berges de l'Oise et d'entreprendre des actions en faveur de la faune et de la flore. Elles ont conduit Maurecourt à remporter le trophée de « Capitale française de la biodiversité 2011 » au concours Natureparif, qui récompense la mise en œuvre de démarches exemplaires en matière de protection et de restauration de la biodiversité.</p>

            </div>
            <!-- Fin - Contenu de la Page -->

            <!-- contenu(s) additionnel(s) dynamique(s) -->
            <aside>

                <!-- bloc contact -->
                <?php
                include '../includes/partial/aside/contact-simple.php';
                ?>

                <!-- Fin - bloc contact -->
            </aside>
            <!-- Fin - contenu(s) additionnel(s) dynamique(s) -->
        </div>
    </div>

<?php
    include '../includes/partial/pages/more.php';
    include '../includes/footer.php';
?>
