<?php
$class='page';
include '../includes/header.php';
include '../includes/menu.html';
?>

<div class="wrap page column1">
    <?php include '../includes/partial/pages/breadcrumb.html'; ?>
    <div class="page-content">
        <h1>Maurecourt</h1>
        <?php include '../includes/partial/pages/share.php'; ?>
        <div class="img-page" style="background-image:url('/assets/images/photo-full.png');"></div>
        <div class="bloc-page">
            <div class="intro">
                <p>Située au nord du département des Yvelines, tout près du confluent de la Seine et de l'Oise, Maurecourt est une commune de 365 hectares nichée entre l'Oise, à l'est, et un plateau agricole abrité par la forêt de l'Hautil, à l'ouest.</p>
            </div>

            <p>Village de 300 habitants en 1900, Maurecourt compte aujourd'hui 4 385 habitants, mais a su garder,  au fil des années, son caractère de « village ». Un habitat à taille humaine, des petits commerces de qualité  en centre-ville et une vie associative, sportive et culturelle d'un remarquable dynamisme en font un lieu  où il fait bon vivre.</p>
            <h2>" Le bon vivre ensemble "</h2>
            <p>La vie locale est rythmée par de nombreuses manifestations, parmi lesquelles les célèbres Gargantuades de printemps, un événement où l'art, l'écriture et la gastronomie se rencontrent autour d'un marché du terroir, d'un festival de musique et d'un salon du livre gourmand.
                Par ailleurs, le village de Maurecourt invite à la promenade. Ainsi, on peut marcher sur les traces de la peintre impressionniste Berthe Morisot, qui y a séjourné et peint une quinzaine de toiles, marquées de l'empreinte du village. On peut aussi flâner sur les rives de l'Oise pour découvrir l'attachement de Maurecourt à la batellerie. En effet, jusqu'en 1986, la commune avait une activité de réparation et de construction navale. En 2004, la municipalité a décidé de reconquérir les berges de l'Oise et d'entreprendre des actions en faveur de la faune et de la flore. Elles ont conduit Maurecourt à remporter le trophée de « Capitale française de la biodiversité 2011 » au concours Natureparif, qui récompense la mise en œuvre de démarches exemplaires en matière de protection et de restauration de la biodiversité.</p>
            <div class="gallery">
                <div id="grid-container">
                    <a class="example-image-link" href="/assets/images/actu-1.png" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                        <img class="grid-item" src="/assets/images/actu-1.png">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/1200/600?random" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
                        <img class="grid-item" src="https://unsplash.it/1200/600?random">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/800/500?random" data-lightbox="example-set">
                        <img class="grid-item" src="https://unsplash.it/800/500?random">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/600/1200?random" data-lightbox="example-set">
                        <img class="grid-item" src="https://unsplash.it/600/1200?random">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/600/800?random" data-lightbox="example-set">
                        <img class="grid-item" src="https://unsplash.it/600/800?random">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/800/450?random" data-lightbox="example-set">
                        <img class="grid-item" src="https://unsplash.it/800/450?random">
                    </a>
                    <a class="example-image-link" href="https://unsplash.it/1000/800?random" data-lightbox="example-set">
                        <img class="grid-item" src="https://unsplash.it/1000/800?random">
                    </a>
                </div>
            </div>
        </div>
        <aside>
            <div class="bloc bloc-contact">
                <h3>CONTACT</h3>
                <p><em>Mairie de Maurecourt</em><br/>
                     1 rue du Maréchal Leclerc<br/>
                    78780 MAURECOURT</p>

                <p class="phone"><span>01 39 70 23 20</span></p>
                  <p><a href="http://www.ville-maurecourt.fr" class="website" target="_blank">www.ville-maurecourt.fr</a></p>
            </div>
        </aside>
    </div>
</div>


<?php
include '../includes/partial/pages/more.php';
include '../includes/footer.php';
?>
