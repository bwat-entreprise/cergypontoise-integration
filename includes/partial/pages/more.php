<!-- Pages liées -->


<!--
BWAT IG - Pages liées  :

On  affiche le titre de la page
L'image utilisée est le format vignette (360px par 240px)

Il n'y a pas de limite sur le nombre de pages liées

-->
<div class="more">
    <div class="more-content wrap">
        <h2>À lire aussi...</h2>
        <div class="item">
            <a href="/lien-exemple">
                <span class="item-more" style="background-image:url('/assets/images/more/example-1.png');"></span>
            </a>
            <p><a href="/lien-exemple">Roméo Elvis + Un d’chaque</a></p>
        </div>

        <div class="item">
            <a href="/lien-exemple">
                <span class="item-more" style="background-image:url('/assets/images/more/example-2.png');"></span>
            </a>
            <p><a href="/lien-exemple">Roméo Elvis + Un d’chaque</a></p>
        </div>

        <div class="item">
            <a href="/lien-exemple">
                <span class="item-more" style="background-image:url('/assets/images/more/example-3.png');"></span>
            </a>
            <p><a href="/lien-exemple">Roméo Elvis + Un d’chaque</a></p>
        </div>
    </div>
</div>