<!-- BWAT IG : bloc contact
Une page peut être liée à un ou plusieurs contact
Un contact c'est :
    Un nom de structure
    Et/ou un nom de personne, un prénom, une civilité (Mr ou Mme)
    Un nom de fonction (optionnel)
    Une addresse (optionnel)
    Un code postal (optionnel)
    Une ville (optionnel)
    Un téléphone (optionnel)
    Un mail (optionnel)
    Une URL de lien (optionnel)
    Un libellé pour le lien (optionnel)

-->

<div class="bloc bloc-contact">

    <h3>CONTACT</h3>
    <p>
        <em class="contact-structure">Mairie de Maurecourt</em><br/>
        <span class="contact-name">Mme Prénom Nome</span><br/>
        <span class="contact-function">Secrétaire</span><br/>
        <span class="contact-address">
             1 rue du Maréchal Leclerc<br/>
            <span class="zip">78780</span> <span class="city">MAURECOURT</span>
        </span>
    </p>
    <p class="phone"><span><a href="tel:0139702320">01 39 70 23 20</a></span></p>
    <!-- BWAT IG : important, crypter au max le mail (obfuscate)  -->
    <p class="mail"><a href="mailto:mail@example.com">mail@example.com</a></p>
      <p><a href="http://www.ville-maurecourt.fr" class="website" target="_blank">www.ville-maurecourt.fr</a></p>
</div>