<!-- BWAT IG : info contextuelle
Une page peut être liée à un ou plusieurs blocs d'info contextuelle
Une info contextuelle c'est :
    - un titre
    - un texte
-->
<div class="bloc bloc-bon-a-savoir">
    <h3>bon à savoir</h3>
    <p>Pendant les vacances scolaires, l’agglomération propose un tarif préférentiel pour les enfants et les adolescents : l’entrée est à 1 euro pour les moins de 16 ans tous les après-midis à partir de 14h. Et pour les moins de 6 ans, l’entrée est toujours gratuite tout au long de l’année.</p>
</div>