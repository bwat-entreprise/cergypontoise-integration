<!-- BWAT IG : downloads
Une page peut être liée à un ou plusieurs téléchargement
Une téléchargement c'est :
    - un titre
    - un fichier uploadé via le backend
Le type, ici toutes les occurences de "pdf", peut-être extrait de l'extension du fichier.
-->
<div class="bloc bloc-telecharger">
    <h3>à télécharger</h3>

    <div class="pdf">
        <div class="img">
            <a href="/file.pdf" target="_blank"><img src="/assets/images/picto-pdf.png" alt="picto pdf"/></a>
        </div>
        <div class="text">
            <p><a href="/file.pdf" target="_blank">Consulter les tarifs</a></p>
            <em>Pdf - 30,44 Ko</em>
        </div>
    </div>

    <div class="pdf">
        <div class="img">
            <a href="/file.pdf" target="_blank"><img src="/assets/images/picto-pdf.png" alt="picto pdf"/></a>
        </div>
        <div class="text">
            <p><a href="/file.pdf" target="_blank">Consulter les tarifs</a></p>
            <em>Pdf - 30,44 Ko</em>
        </div>
    </div>

    <div class="pdf">
        <div class="img">
            <a href="/file.pdf" target="_blank"><img src="/assets/images/picto-pdf.png" alt="picto pdf"/></a>
        </div>
        <div class="text">
            <p><a href="/file.pdf" target="_blank">Consulter les tarifs</a></p>
            <em>Pdf - 30,44 Ko</em>
        </div>
    </div>
</div>