<!-- BWAT IG : bloc contact avec plan

    Identique à includes/partial/aside/contact-simple.php
    Mais avec une classe différente et l'ajout d'un module de carte

-->
<div class="bloc bloc-address"> <!-- BWAT IG : la classe bloc-address remplace bloc-contact -->
    <h3>Adresse</h3>
    <p>
        <em class="contact-structure">Mairie de Maurecourt</em><br/>
        <span class="contact-name">Mme Prénom Nome</span><br/>
        <span class="contact-function">Secrétaire</span><br/>
        <span class="contact-address">
             1 rue du Maréchal Leclerc<br/>
            <span class="zip">78780</span> <span class="city">MAURECOURT</span>
        </span>
    </p>

    <p class="phone"><span><a href="tel:0139702320">01 39 70 23 20</a></span></p>
    <!-- BWAT IG : important, crypter au max le mail (obfuscate)  -->
    <p class="mail"><a href="mailto:mail@example.com">mail@example.com</a></p>

      <p><a href="http://www.ville-maurecourt.fr" class="website" target="_blank">www.ville-maurecourt.fr</a></p>

    <!-- BWAT IG : Ajout d'un module de carte avec le lieu géolocalisé -->
    <!-- map -->
    <div class="contact-map">
        <img src="/assets/images/map-page.png" alt="map page"/>
        <!-- lien vers la carte en pleine page ou google maps -->
        <a href="" target="_blanck">
           <span class="button">
               <span>VOIR EN LIGNE</span>
           </span>
        </a>
    </div>
    <!-- fin - map -->
</div>