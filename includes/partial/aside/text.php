<!-- BWAT IG : texte contextuel
Une page peut être liée à un texte contextuel
Un texte contextuel c'est :
    - un titre
    - un texte
    - une URL (lien)
    - une libellé pour le lien (ici : "Voir en ligne")
    - une target pour le lien (enum "_blank" ou "_parent")

-->
<div class="bloc bloc-text">

    <!-- titre -->
    <h3>TITRE DU BLOC</h3>

    <!-- texte -->
    <ul>
        <li>Située au nord du département des Yvelines, tout près du confluent de la Seine et de l'Oise, Maurecourt est une commune de 365 hectares nichée entre l'Oise, à l'est, et un plateau agricole abrité par la forêt de l'Hautil, à l'ouest.</li>
        <li>Située au nord du département des Yvelines, tout près du confluent de la Seine et de l'Oise, Maurecourt est une commune de 365 hectares nichée entre l'Oise, à l'est, et un plateau agricole abrité par la forêt de l'Hautil, à l'ouest.</li>
    </ul>
    <!-- lien -->
    <a href="/lien" target="_parent">
       <span class="button">
           <span>VOIR EN LIGNE</span>
       </span>
    </a>
</div>