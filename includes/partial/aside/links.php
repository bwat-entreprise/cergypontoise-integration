<!-- BWAT IG : links
Une page peut être liée à un ou plusieurs liens externes
Un lien c'est :
    - une URL
    - un libellé (ici "www.site-internet.fr")

-->
<div class="bloc bloc-web">
    <h3>SUR LE WEB</h3>
    <ul>
        <li><a href="http://example.com" target="_blank">www.site-internet.fr</a></li>
        <li><a href="http://example.com" target="_blank">www.site-internet.fr</a></li>
    </ul>
</div>