<!-- Footer -->
<footer>
    <div class="wrap">
        <div class="column">
            <a href="/index">
                <img src="/assets/images/cergy-footer.png" alt="logo cergy pontoise agglomération"/>
            </a>
            <div class="footer-text">
                <h4>COMMUNAUTE D’AGGLOMERATION<br/>
                     DE CERGY-PONTOISE</h4>
                <h5>Hôtel d'agglomération</h5>
                <p>Parvis de la Préfecture - CS 80309<br/>
                    95027 Cergy-Pontoise Cedex<br/>
                    Tél. 01 34 41 42 43</p>
            </div>
        </div>

        <div class="column column2">
            <div class="bloc-footer">
                <!-- Footer info 1 -->
                <!-- BWAT IG - Idéalement il faudrait que ce texte soit éditable via le backend -->
                <h5>Horaires </h5>
                <p>du lundi au jeudi  8h30 - 12h30 et 13h30 - 18h,  <br/>
                le vendredi 8h30 - 12h30 et 13h30 - 17h30</p>
                <!-- Fin - Footer info 1 -->
            </div>
            <div class="bloc-footer2">
                <!-- Footer info 2 -->
                <!-- BWAT IG - Idéalement il faudrait que ce texte soit éditable via le backend -->
                <h5>Accueil du Verger </h5>
                <p>Rue de la gare - 95027 Cergy<br/>
                Tél. 01 34 41 42 43</p>
                <h5>Horaires </h5>
                <p>du lundi au jeudi 8h30 - 12h30 et 13h30 - 18h,  <br/>
                le vendredi 8h30 - 12h30 et 13h30 - 17h30</p>
                <!-- Fin - Footer info 2 -->
            </div>
        </div>
        <div class="column right-footer">
            <!-- Network footer -->

            <!-- BWAT IG - network footer
            Un lien c'est :
                - Une URL
                - Un libelle (ex : "Facebook")
                - Un icone PNG 35 x 35px

            -->
            <ul class="social">
                <li>
                    <a href="https://www.facebook.com/CergyPontoiseAgglo/" target="_blank">
                        <img src="/assets/images/footer/facebook.png" alt="facebook"/>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/cergypontoise95?lang=fr" target="_blank">
                        <img src="/assets/images/footer/twitter.png" alt="twitter"/>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/cergypontoise_agglo/" target="_blank">
                        <img src="/assets/images/footer/instagram.png" alt="instagram"/>
                    </a>
                </li>
                <li>
                    <a href="https://fr.linkedin.com/company/communaut-d%27agglom-ration-de-cergy-pontoise" target="_blank">
                        <img src="/assets/images/footer/linkedin.png" alt="linkedin"/>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UCA38uy909-JDE282eFblOzQ" target="_blank">
                        <img src="/assets/images/footer/youtube.png" alt="youtube"/>
                    </a>
                </li>
                <li>
                    <a href="http://www.13commeune.fr/actualite/une-appli-100-agglo" target="_blank">
                        <img src="/assets/images/footer/appli.png" alt="appli"/>
                    </a>
                </li>
            </ul>
            <!-- Fin - network footer -->

            <!-- Apps -->
            <!-- BWAT IG : ça peut rester en statique -->
            <p>Télécharger nos applis</p>
            <div class="stores">
                <a href="https://www.microsoft.com/fr-fr/store/apps/windows?icid=TopNavSoftwareWindowsApps" target="_blank">
                    <img src="/assets/images/footer/windows.png" alt="windows store"/>
                </a>
                <a href="https://play.google.com/store" target="_blank">
                    <img src="/assets/images/footer/google-play.png" alt="google play"/>
                </a>
                <a href="https://www.apple.com/fr/" target="_blank">
                    <img src="/assets/images/footer/apple-store.png" alt="App Store"/>
                </a>
            </div>
            <div class="apps">
                <a href="/applications">
                    <img src="/assets/images/footer/sortir-application.png" alt="sortir application"/>
                    <img src="/assets/images/footer/cergy.png" alt="cergy-pontoise application"/>
                </a>
            </div>
            <!-- fin -Apps -->
        </div>

        <!-- Menu footer -->
        <!-- BWAT IG : un menu vers des pages du site -->
        <div class="menu-footer">

            <ul>
                <li><a href="/">Accueil</a></li>
                <li><a href="/page">&gt; Contact</a></li>
                <li><a href="/page">&gt; Mentions légales</a></li>
                <li><a href="/page">&gt; Crédits</a></li>
                <li><a href="/page">&gt; Accessibilité</a></li>
                <li><a href="/page">&gt; Déclaration de conformité</a></li>
                <li><a href="/page">&gt; Plan du site</a></li>
            </ul>
        </div>
        <!-- Fin - Menu footer -->
    </div>
</footer>

<?php include 'cookies.html' ?>

<script src="../assets/dist/all.js"></script>


<script>
    $('.partenary .slider-nav .content').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        dots: false,
        centerMode: false,
        focusOnSelect: false,
        arrows: true,
        appendArrows: '.partenary .slider-nav'
    });

    $('.access .slider ul').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        focusOnSelect: false,
        arrows: true,
        appendArrows: '.access .slider ul'
    });

    var parameters = {
        gridContainer: '#grid-container',
        gridItems: '.grid-item',
        gutter: '10',
        enableImagesLoaded: false
    };
    var grid = new justifiedGrid(parameters);
    $('body').imagesLoaded( function() {
        grid.initGrid();
    });

</script>

<?php
/*
// Social stream en cours
?>
<script src="../assets/js/jquery.social.stream.wall.1.8.js"></script>
<script src="../assets/js/jquery.social.stream.1.6.2.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#social-stream').dcSocialStream({
            feeds: {
                twitter: {
                    id: '#CergyPontoise95',
                    thumb: true
                },
                facebook: {
                    id: '133439590057234'
                },
                youtube: {
                    id: 'UUA38uy909-JDE282eFblOzQ'
                },
                instagram: {
                    id: '!3447430596',
                    accessToken: '3447430596.b5799d6.eefcce6f2a2c4175af6f409c172f15bc'
                }
            },
            rotate: {
                delay: 0
            },
            twitterId: 'CergyPontoise95',
            control: true,
            filter: true,
            wall: true,
            order: 'date',
            max: 1,
            iconPath: '/assets/images/social/',
            imagePath: '/assets/images/social/'
        });
    });
</script>
*/
?>


</body>
</html>