<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Agglomération de Cergy</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link rel="stylesheet" href="../assets/dist/justified.css">
    <link rel="icon" href="/favicon.ico" />
    <link rel="stylesheet" href="../assets/dist/lightbox.min.css">
    <link rel="stylesheet" href="../assets/css/dcsns_wall.css">
    <link rel="stylesheet" href="../assets/dist/styles.css">
    <link rel="stylesheet" href="../assets/dist/slick.css">
</head>
<body id="<?php echo $class ?>" >

<!-- message flash -->
<div class="top-text">
    <div class="wrap">
        <img class="info" src="../assets/images/info-top.png" alt="info icône top"/>
        <p>Texte de message flash…. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam </p>
    </div>
    <div id="close" class="close"><p>Fermer</p></div>
</div>
<!-- fin - message flash -->


<div class="wrap social-box">
    <div class="social">
        <!-- Menu socials -->
        <!-- BWAT IG : un menu vers des liens externes et des pages du site -->
        <ul>
            <li>
                <a href="https://www.facebook.com/CergyPontoiseAgglo/" class="facebook" target="_blank"><span>Facebook</span></a>
            </li>
            <li>
                <a href="https://twitter.com/cergypontoise95?lang=fr" class="twitter" target="_blank"><span>Twitter</span></a>
            </li>
            <li>
                <a href="https://www.instagram.com/cergypontoise_agglo/" class="instagram" target="_blank"><span>Instagram</span></a>
            </li>
            <li>
                <a href="https://fr.linkedin.com/company/communaut-d%27agglom-ration-de-cergy-pontoise" class="linkedin" target="_blank"><span>Linked in</span></a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCA38uy909-JDE282eFblOzQ" class="youtube" target="_blank"><span>Youtube</span></a>
            </li>
            <li>
                <a href="/applications" target="_blank" class="appli"><span>13 communes</span></a>
            </li>
        </ul>
        <!-- Fin - Menu socials -->
    </div>
</div>